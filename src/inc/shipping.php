<?php

class Shipping{
    
    private $api = 'https://auspost.com.au/api/';
	private $service_code = 'AUS_PARCEL_REGULAR';
    private $api_key = 'xxx';
    
    private function getRemoteData($url){
        $ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Auth-Key: ' . $this->api_key
                    ));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$contents = curl_exec($ch);
		curl_close($ch);
		return json_decode($contents,true);
    }    
    
    private function arrayToUrl($url,$array){
		$first = true;
		foreach ($array as $key => $value){
        	$url .= $first ? '?' : '&';
           	$url .= "{$key}={$value}";
            $first = false; 	
		}	
		return $url;
    }
    
    public function checkPostCode($suburb, $postcode, $state){
        $sp = $suburb . " " . $postcode;
        $data = array(
                'q' => $sp,
                'state' => $state
        );
        $edeliver_url = "{$this->api}postcode/search.json";
        $edeliver_url = $this->arrayToUrl($edeliver_url, $data);
        $results = $this->getRemoteData($edeliver_url);
 
		if (isset($results['error'])){
        	throw new Exception($results['error']['errorMessage']);
        }
            
        return count($results['localities'])> 0 ? true : false;
    }
	
	public function getShippingCosts($from_postcode, $to_postcode, 
									 $lenght, $width, 
									 $height){
		$data = $array(
				'from_postcode' => $from_postcode,
				'to_postcode' => $to_postcode,
				'lenght' => $lenght,
				'width' => $width,
				'height' => $height,
				'service_code' => $this->service_code);
		$edeliver_url = "{$this->api}postage/parcel/
						domestic/calculate.json";
		$edeliver_url = $this->arrayToUrl($edeliver_url, $data);
		$results = $this=>getRemoteData($edeliver_url);
		
		if (isset($results['error'])){
            throw new Exception($results['error']['errorMessage']);
        }
		
		return $results['postage_result']['total_cost'];										 																					
	}
    
}

